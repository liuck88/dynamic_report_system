package com.liuck.reportsys.controller.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.liuck.reportsys.bean.role.Role;
import com.liuck.reportsys.impl.RoleService;

@RestController
@RequestMapping("/role/*")
public class RoleController {

	@Autowired
	private RoleService roleService;
	
	@GetMapping("getRoleById")
	public Role getRoleById(Integer roleId) {
		
		return roleService.getRoleById(roleId);
	}
}
