package com.liuck.reportsys.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.liuck.reportsys.bean.user.User;
import com.liuck.reportsys.impl.UserService;

@RestController
@RequestMapping("/user/*")
public class UserController {
	
	@Autowired
	private UserService userService;

	@RequestMapping("getUserById")
	public User getUserById(Integer userId) {
		
		return userService.getUserById(userId);
	}
	
}
