package com.liuck.reportsys.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.liuck.reportsys.bean.user.User;
import com.liuck.reportsys.mapper.UserMapper;

@Service
public class UserService {
	
	Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserMapper userMapper;
	
	public User getUserById(Integer userId) {
		User user = userMapper.getUserById(userId);
		return user;
	}
	
	public User getUserByCode(String usercode) {
		User user = userMapper.getUserByCode(usercode);
		return user;
	}
	
}
