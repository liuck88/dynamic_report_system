package com.liuck.reportsys.impl;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import com.liuck.reportsys.quartz.factory.SchedulerJob;

@Service
public class QuartzService {
	
	@Autowired
	private SchedulerFactoryBean schedulerFactoryBean;
	
	private Logger logger = LoggerFactory.getLogger(QuartzService.class);

	/**
	 * 添加定时任务
	 * @author liuck
	 * @since  2019年1月5日
	 * @param jobName
	 * @param jobGroup
	 * @param jobDesc
	 * @throws SchedulerException
	 */
	public void addCronJob(String jobName, String jobGroup, String jobDesc){
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		
		JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
		try {
			JobDetail detail = scheduler.getJobDetail(jobKey);
			if(detail != null) {
				logger.info("定时任务："+jobName+" 已存在");
			}else {
				//创建jobDetail
				JobDetail jobDetail = JobBuilder.newJob(SchedulerJob.class)
						.withIdentity(jobName, jobGroup)
						.usingJobData("jobDesc", jobDesc)
						.build();
				
				//创建trigger
				Trigger trigger = TriggerBuilder.newTrigger()
						.withIdentity(jobName, jobGroup)
						.withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
						.build();
				
				scheduler.start();
				
				scheduler.scheduleJob(jobDetail, trigger);
			}
			logger.info("添加任务成功");
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * 添加异步任务
	 * @author liuck
	 * @since  2019年1月5日
	 * @param jobName
	 * @param jobGroup
	 */
	public void addAsyncJob(String jobName, String jobGroup) {
		
	}
	
	/**
	 * 暂停定时任务
	 * @author liuck
	 * @since  2019年1月5日
	 * @param triggerName
	 * @param triggerGroup
	 */
	public void pauseJob(String triggerName, String triggerGroup) {
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		
		TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroup);
		try {
			scheduler.pauseTrigger(triggerKey);
			logger.info("暂停任务成功！");
		} catch (SchedulerException e) {
			logger.info("暂停任务失败！");
			e.printStackTrace();
		}
	}
	
	/**
	 * 恢复暂停的任务
	 * @author liuck
	 * @since  2019年1月5日
	 * @param triggerName
	 * @param triggerGroup
	 */
	public void resumeJob(String triggerName, String triggerGroup) {
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroup);
		try {
			scheduler.resumeTrigger(triggerKey);
			logger.info("恢复任务成功");
		} catch (SchedulerException e) {
			logger.info("恢复任务失败");
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除任务
	 * @author liuck
	 * @since  2019年1月5日
	 * @param jobName
	 * @param jobGroup
	 */
	public void deleteJob(String jobName, String jobGroup) {
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
		try {
			scheduler.deleteJob(jobKey);
			logger.info("删除任务成功");
		} catch (SchedulerException e) {
			logger.info("删除任务失败");
			e.printStackTrace();
		}
	}
}
