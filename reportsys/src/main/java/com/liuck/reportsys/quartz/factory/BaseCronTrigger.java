package com.liuck.reportsys.quartz.factory;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class BaseCronTrigger{
	
	private JobDetail jobDetail;
	
	private CronTrigger cronTrigger;
	
	public void init() {
		JobDetail jobDetail = JobBuilder.newJob(this.getSchedulerJob().getClass())
				.withIdentity(this.getClass().getSimpleName(),this.getClass().getName())
				.usingJobData("jobDesc", this.getJobDesc())
				.build();
		this.setJobDetail(jobDetail);
		
		Trigger trigger = TriggerBuilder.newTrigger()
				.withIdentity(this.getClass().getSimpleName(), this.getClass().getName())
				.withSchedule(CronScheduleBuilder.cronSchedule(this.getConExpression()))
				.build();
		this.setCronTrigger((CronTrigger)trigger);
	}
	
	public Job getSchedulerJob() {
		return new SchedulerJob();
	}
	
	public String getConExpression() {
		return "";
	}
	
	public String getJobDesc() {
		return "";
	}

	public JobDetail getJobDetail() {
		return jobDetail;
	}

	public void setJobDetail(JobDetail jobDetail) {
		this.jobDetail = jobDetail;
	}

	public CronTrigger getCronTrigger() {
		return cronTrigger;
	}

	public void setCronTrigger(CronTrigger cronTrigger) {
		this.cronTrigger = cronTrigger;
	}
	
}
