package com.liuck.reportsys.quartz.factory;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;

public class EmailCronTrigger extends BaseCronTrigger{

	public EmailCronTrigger() {
		init();
	}

	@Override
	public String getConExpression() {
		//每天上午九点执行
		return "0 0 9 * * ?";
	}

	@Override
	public String getJobDesc() {
		return "emailJob";
	}

	@Override
	public JobDetail getJobDetail() {
		return super.getJobDetail();
	}

	@Override
	public CronTrigger getCronTrigger() {
		return super.getCronTrigger();
	}

	
}
