package com.liuck.reportsys.quartz.factory;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchedulerJob implements Job{
	
	Logger log = LoggerFactory.getLogger(SchedulerJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap map = context.getJobDetail().getJobDataMap();
		//根据不同的map实现不同的job任务
		System.out.println("执行的任务："+map.get("jobDesc")+" 执行时间："+formatDate("yyyy年MM月dd日 HH:mm:ss")+" 精确到毫秒："+System.currentTimeMillis());
	}
	
	private String formatDate(String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String date = sdf.format(new Date());
		return date;
	}

}
