package com.liuck.reportsys;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *  程序入口
 * @author liuck
 * @since 2018年11月28日
 */
@SpringBootApplication
@MapperScan("com.liuck.reportsys.mapper")
@EnableAutoConfiguration
public class ReportSysApplication {
    public static void main( String[] args ){
        SpringApplication.run(ReportSysApplication.class, args);
    }
}
