package com.liuck.reportsys.bean.role;

import java.util.List;

import com.liuck.reportsys.bean.resource.Resource;

public class Role {

	private Integer id;
	
	private String code;
	
	private String name;
	
	private String remark;
	
	private List<Resource> roleResources;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<Resource> getRoleResources() {
		return roleResources;
	}

	public void setRoleResources(List<Resource> roleResources) {
		this.roleResources = roleResources;
	}
	
}
